Asteroids
=========

Asteroids is a browser-based version of the classic arcade game written in JavaScript and HTML5. It was created by David Park and Abe Schonfeld. 

Objects in the game move according to Newtonian physics (i.e. they continue to drift in the current direction after acceleration stops).  The games keyboard controls were implemented using event handlers tied to asynchronous timers that refresh the screen. This allows for controls that respond smoothly even when multiple keys are pressed simultaneously. 